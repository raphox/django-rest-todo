## Run

```
source venv/bin/activate && python manage.py runserver $IP:$PORT
```

## Test

```
source venv/bin/activate && python manage.py test
```