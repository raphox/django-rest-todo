from .models import Todo
from .serializers import TodoSerializer, TodoSerializerMinimal

from rest_framework import generics

class TodoList(generics.ListCreateAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    
    def post(self, request, *args, **kwargs):
        """
        Create a new todo
        ---
        omit_parameters:
            - form
        parameters:
            - name: body
              pytype: TodoSerializerMinimal
              paramType: body
        """
        return self.create(request, *args, **kwargs)


class TodoDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Todo.objects.all()
    serializer_class = TodoSerializer
    
    def put(self, request, *args, **kwargs):
        """
        Update a exists todo
        ---
        omit_parameters:
            - form
        parameters:
            - name: body
              pytype: TodoSerializerMinimal
              paramType: body
        """
        return self.update(request, *args, **kwargs)