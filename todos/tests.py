import json
from collections import OrderedDict

from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.test import override_settings, modify_settings
from django.test.client import encode_multipart, RequestFactory

from rest_framework import status, pagination
from rest_framework.test import APITestCase

from .models import Todo
from .serializers import TodoSerializer

@override_settings(SECURE_SSL_REDIRECT=False)
class AccountTests(APITestCase):
    def setUp(self):
        user = User.objects.create_superuser('temporary', 'temporary@gmail.com', 'temporary')
    
    def test_list_todo(self):
        """
        Ensure we can list all todos object.
        """
        self.client.login(username='temporary', password='temporary')
        
        for i in xrange(1, 16):
            Todo.objects.create(title="Todo Test {0}".format(i))

        url      = reverse('todo-list')
        response = self.client.get(url, { 'limit': 10 })
        
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, OrderedDict([
            ('count', 15),
            ('next', "http://testserver{0}?limit=10&offset=10".format(url)),
            ('previous', None),
            ('results',  TodoSerializer(Todo.objects.all()[:10], many=True).data)
        ]))

    def test_create_todo(self):
        """
        Ensure we can create a new todo object.
        """
        self.client.login(username='temporary', password='temporary')

        url   = reverse('todo-list')
        data  = {
            'title': 'Todo Test 1'
        }
        
        response = self.client.post(url, data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data, { 'id': 1, 'title': 'Todo Test 1' })
    
    def test_update_todo(self):
        """
        Ensure we can update a exists todo object.
        """
        self.client.login(username='temporary', password='temporary')

        todo = Todo.objects.create(title="Todo Test 1")

        url   = reverse('todo-detail', kwargs={ 'pk': 1 })
        data  = {
            'title': 'Todo Test 1 (updated)'
        }
        
        response = self.client.put(url, data)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, { 'id': 1, 'title': 'Todo Test 1 (updated)' })
    
    def test_delete_todo(self):
        """
        Ensure we can delete a exists todo object.
        """
        self.client.login(username='temporary', password='temporary')

        todo = Todo.objects.create(title="Todo Test 1")

        url   = reverse('todo-detail', kwargs={ 'pk': 1 })
        
        response = self.client.delete(url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(response.data, None)
